import java.io.{FileInputStream, FileOutputStream}
import scala.io.Source

import scala.collection.mutable.ArrayBuffer
import scala.io.StdIn.{readFloat, readInt, readLine}

object StudentsCRUD {
  private val students = ArrayBuffer.empty[Student]

  def printMenu(): Unit = {
    println("""Menu:
      |1) Show all students
      |2) Add new student
      |3) Edit student
      |4) Delete student
      |5) Save students
      |6) Load students
      |7) Exit""".stripMargin)
  }

  def showAllStudents(): Unit = {
    print("Select sorting mode (1=id, 2=name, 3=age, 4=averageMark, default=no sorting): ")

    val sortedStudents = try {
      readInt() match {
        case 1 => students.sortBy(_.id)
        case 2 => students.sortBy(_.name)
        case 3 => students.sortBy(_.age)
        case 4 => students.sortBy(_.averageMark)
        case _ => students
      }
    } catch {
      case _: NumberFormatException => students
    }

    println("ID\t\tName\t\tAge\t\tAverage mark")
    sortedStudents.foreach(s => {
      printf("%d\t\t%s\t\t%d\t\t%f\n", s.id, s.name, s.age, s.averageMark)
    })
  }

  def readStudentData(): Student = {
    print("ID: ")
    val id = readInt()

    print("Name: ")
    val name = readLine()

    print("Age: ")
    val age = readInt()

    print("Average mark: ")
    val averageMark = readFloat()

    Student(id, name, age, averageMark)
  }

  def addNewStudent(): Unit = {
    students.append(readStudentData())
  }

  def editStudent(): Unit = {
    val newStudent = readStudentData()

    students.indexWhere(_.id == newStudent.id) match {
      case -1 => println(s"Student with ID ${newStudent.id} was not found!")
      case i  => students(i) = newStudent
    }
  }

  def deleteStudent(): Unit = {
    print("ID: ")
    val id = readInt()

    students.indexWhere(_.id == id) match {
      case -1 => println(s"Student with ID $id was not found!")
      case i  => students.remove(i)
    }
  }

  def saveStudents(): Unit = {
    print("Enter file name: ")
    val fileName = readLine()

    val fileStream = new FileOutputStream(fileName)
    students.foreach(s => {
      fileStream.write(s"${s.id};${s.name};${s.age};${s.averageMark}\n".getBytes)
    })

    fileStream.close()
    println("Saved!")
  }

  def loadStudents(): Unit = {
    print("Enter file name: ")
    val fileName = readLine()

    students.clear()

    val source = Source.fromFile(fileName)
    for (line <- source.getLines if line != "") {
      val splitted = line.split(';')
      students.append(Student(splitted(0).toInt, splitted(1), splitted(2).toInt, splitted(3).toFloat))
    }

    source.close()
    println("Loaded!")
  }

  def main(args: Array[String]): Unit = {
    while (true) {
      printMenu()

      print("Select item: ")
      readInt() match {
        case 1 => showAllStudents()
        case 2 => addNewStudent()
        case 3 => editStudent()
        case 4 => deleteStudent()
        case 5 => saveStudents()
        case 6 => loadStudents()
        case 7 =>
          println("Bye!")
          System.exit(0)
        case _ =>
      }

      println()
    }
  }
}
